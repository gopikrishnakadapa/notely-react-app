import React from 'react';

function FooterComponent(props) {
    return (
        <div className="container" style={{ marginTop: "45px" }}>
            <hr />
            <footer>
                <p>&copy; 2019 - Notely</p>
            </footer>
        </div>
    );
}

export default FooterComponent;