import React from 'react';
import PropTypes from 'prop-types';

function NoteDetailBodyComponent(props) {
    return (
        <div className="card-body">
            <div className="note-detail__date-section">
                <div>
                    <span className="note-detail__created-date-key">Created On:</span>
                    <span className="note-detail__created-date-value">{props.note !== undefined ? props.note.createdDate : ""}</span>
                </div>
                <div>
                    <span className="note-detail__modified-date-key">Last Modified On:</span>
                    <span className="note-detail__modified-date-value">{props.note !== undefined ? props.note.lastModified : ""}</span>
                </div>
            </div>
            <hr />
            <div>{props.note !== undefined ? props.note.detail : ""}</div>
        </div>
    );
}

NoteDetailBodyComponent.propTypes = {
    note: PropTypes.object
};

export default NoteDetailBodyComponent;
