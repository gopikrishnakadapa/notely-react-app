import React from 'react';
import PropTypes from 'prop-types';

function NoteDetailHeaderComponent(props) {
    return (
        <div className="card-header note-detail__panel-header">
            <h1 className="card-title note-detail__panel-title">
                {props.note !== undefined ? props.note.subject : ""}
            </h1>
        </div>
    );
}

NoteDetailHeaderComponent.propTypes = {
    note: PropTypes.object
};

export default NoteDetailHeaderComponent;
