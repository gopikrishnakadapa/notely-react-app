import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { editNoteActionCreators } from '../../../store/EditNote'; 

class EditNoteComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            subject: "",
            body: "",
            isNoteDirty: false,
            displayFieldError: false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        const note = getNoteFromList(this.props);
        if (note !== undefined) {
            this.setState({ subject: note.subject, body: note.detail});
        }    
    }

    handleChange(event) {
        const target = event.target;
        if (target.id ==="subject") {
            this.setState({ subject: target.value, isNoteDirty:true });
        } else {
            this.setState({ body: target.value, isNoteDirty:true });
        }
    }
    
    handleSubmit(note) {
        if (this.isPageValid()) {
            let subject = this.state.subject;
            let body = this.state.body;
            let editedNote = {id: note.id, subject: subject, detail: body }
           
            this.props.editNote(editedNote);
            this.props.history.push("/");
        } else {
            this.setState({ displayFieldError : true });
        }
    }

    isPageValid() {
      return (this.state.subject !== "" && this.state.isNoteDirty);
    }

    render() {
        const note = getNoteFromList(this.props);
        return (
            <div className = "container">
            <div className="note-editor">
                <div className="card note-editor__panel">
                    <div className="card-header note-editor__header">
                        <h1 className="card-title note-editor__panel-title">
                            <span>Edit Your Note</span>
                        </h1>
                    </div>
                    <form>
                        <div className="card-body note-editor__panel-body">                              
                            <div className="form-group">
                                <label className="note-editor__input-label">Subject</label>
                                <input className="form-control note-editor__subject-input"
                                id="subject"  
                                defaultValue =  {note !== undefined ? note.subject:""} 
                                onChange ={this.handleChange}/>
                            </div>
            
                            <div className="form-group">
                                <label className="note-editor__input-label" >Your Note</label>
                                <textarea className="form-control note-editor__detail-input" 
                                id="body" 
                                rows="12" 
                                cols="20" 
                                defaultValue = {note !== undefined?note.detail: ""} 
                                onChange ={this.handleChange}></textarea>
                            </div>
                        </div>
                        <div className="card-footer">
                            <div className="note-editor__flex-container">
                                <a className="btn btn-sm note-editor__button" href="/">Cancel</a>
                                <button className="btn btn-sm note-editor__button" type="button" onClick = {() => this.handleSubmit(note)}>Save Note</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        );
    }
}

function getNoteFromList(props) {
    let notes = props.notes;
    if (notes.length > 0) {
        let id = props.match.params.id;
        let note = notes.find(note => note.id == id);
        return note;
    }
}

function mapStateToProps(state) {
    return {

    };
}

function mapDispatchToProps(dispatch) {
    return  bindActionCreators({
        editNote: editNoteActionCreators.editNote
     }, dispatch)  
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditNoteComponent);
