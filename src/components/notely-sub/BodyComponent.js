import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from "react-router-dom";
import NoteListComponent from './body-sub/NoteListComponent';
import NoteDetailComponent from './body-sub/NoteDetailComponent';
import NewNoteComponent from './body-sub/NewNoteComponent';
import EditNoteComponent from './body-sub/EditNoteComponent';

class BodyComponent extends Component {
    constructor(props) {
        super(props)  
    }

    render() {
        let notes = this.props.noteState.notes;
        return (
            <Switch>
                <Route exact path="/" render={() => <NoteListComponent  notes = {notes}/>} />
                <Route exact path="/create-note" render= {(props) => <NewNoteComponent {...props}/>} />
                <Route exact path="/note-detail/:id" render={(props) => <NoteDetailComponent {...props}  notes = {notes}/>} />
                <Route exact path="/edit-note/:id" render={(props) => <EditNoteComponent {...props} notes = {notes}/>} />              
            </Switch>
        );
    }
}


function mapStateToProps(state) {
    return {
        noteState: state.noteList
    };
}

function mapDispatchToProps(dispatch) {
    return {

    };
}

export default connect(
    mapStateToProps,
)(BodyComponent);
