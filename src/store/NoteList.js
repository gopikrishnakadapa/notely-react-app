const requestNoteListType = 'Request_NoteList';
const receiveNoteListSuccessType = 'Receive_NoteList_Success';
const receiveNoteListErrorType = 'Receive_NoteList_Success';
const addNoteToNoteListType = 'Add_Note_To_NoteList';
const removeNoteFromNoteListType = 'Remove_Note_From_NoteList';
const updateNoteListType = 'Update_NoteList';

const initialState = { notes: [], isloading: false, isError: false};

export const noteListActionCreators = {
  addNoteToNoteList: (note) => ({ type: addNoteToNoteListType, note }),
  removeNoteFromNoteList: (id) => ({ type: removeNoteFromNoteListType, id }),
  updateNoteList: (note) => ({ type: updateNoteListType, note }),

  fetchNotes: () => async (dispatch, getState) => {

    dispatch({ type: requestNoteListType });

    const url = `/api/note`;

    const response = await fetch(url,
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      });

    if (response.ok) {
      const notes = await response.json();
      dispatch({ type: receiveNoteListSuccessType, notes });
    } else {
      dispatch({ type: receiveNoteListErrorType });
    }
  }
};

export const reducer = (state, action) => {

  state = state || initialState;

  if (action.type === requestNoteListType) {
    return {
      ...state,
      isloading: true
    };
  }

  if (action.type === receiveNoteListSuccessType) {
    return {
      ...state,
      notes: action.notes,
      isloading: false,
      isError:false
    };
  }
  if (action.type === receiveNoteListErrorType) {
    return {
      ...state,
      isloading: false,
      isError:true
    };
  }
  if (action.type === addNoteToNoteListType) {
    return {
      ...state,
      notes: [...state.notes, action.note]
    };
  }
  if (action.type === removeNoteFromNoteListType) {
    let notes = state.notes.filter(note => note.id !== action.id)
    return {
        ...state,
        notes: notes
    };
  }

  if (action.type === updateNoteListType) {
       let notes =  state.notes;
       let index = notes.findIndex(note => note.id === action.note.id);
       notes[index] = action.note;
    return {
        ...state,
        notes: [...notes]
    };
  }

  return state;
};

