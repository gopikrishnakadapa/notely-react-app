import {noteListActionCreators} from './NoteList';
const deleteNoteRequestType = 'EditNote_Request';
const deleteNoteResponseSuccessType = 'EditNote_Response_Success';
const deleteNoteResponseErrorType = 'EditNote_Response_Error';
const clearNoteType = 'Clear_Note';
const initialState = {note: null, isError: false, isloading : false};

export const deleteNoteActionCreators = {
    deleteNote: (noteId) => async (dispatch, getState) => {

        dispatch({ type: clearNoteType });

        dispatch({ type: deleteNoteRequestType });

        const url = `/api/note/${noteId}`;

        const response = await fetch(url,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            });

        if (response.ok) {
            const note = await response.json(); 
            const noteId = note.id;    
            dispatch({ type: deleteNoteResponseSuccessType, note });
            dispatch(noteListActionCreators.removeNoteFromNoteList(noteId));
        } else {
            dispatch({ type: deleteNoteResponseErrorType });
        }   
      
    }
};

export const reducer = (state, action) => {

    state = state || initialState;

    if (action.type === deleteNoteRequestType) {
        return {
            ...state,
            isloading: true
        };
    }

    if (action.type === deleteNoteResponseSuccessType) {
        return {
            ...state,
            note: action.note,
            isloading: false
        };
    }

    if (action.type === deleteNoteResponseErrorType) {
        return {
            ...state,
            isError: true,
            isloading: false
        };
    }

    if (action.type === clearNoteType) {
        return {
            ...state,
            ...initialState
        };
    }

    return state;
};
