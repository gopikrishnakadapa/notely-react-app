import {noteListActionCreators} from './NoteList';

const editNoteRequestType = 'EditNote_Request';
const editNoteResponseSuccessType = 'EditNote_Response_Success';
const editNoteResponseErrorType = 'EditNote_Response_Error';
const clearNoteType = 'Clear_Note';

const initialState = {note: null, isError: false, isloading : false};

export const editNoteActionCreators = {

    editNote: (note) => async (dispatch, getState) => {

        const requestBody = note;
        console.log(note);
        dispatch({ type: clearNoteType });

        dispatch({ type: editNoteRequestType });

        const url = `/api/note`;

        const response = await fetch(url,
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestBody)
            });

        if (response.ok) {
            const note = await response.json();
            console.log(note);  
            dispatch({ type: editNoteResponseSuccessType, note });
            dispatch(noteListActionCreators.updateNoteList(note));
        } else {
            dispatch({ type: editNoteResponseErrorType });
        }   
      
    }
};

export const reducer = (state, action) => {

    state = state || initialState;

    if (action.type === editNoteRequestType) {
        return {
            ...state,
            isloading: true
        };
    }

    if (action.type === editNoteResponseSuccessType) {
        return{
            ...state,
            event: action.note,
            isloading: false
        };
    }

    if (action.type === editNoteResponseErrorType) {
        return {
            ...state,
            isError: true,
            isloading: false
        };
    }

    if (action.type === clearNoteType) {
        return{
            ...state,
            ...initialState
        };
    }

    return state;
};

